const axios = require('axios');
const qs = require('querystring');

export function getPostByCategory(category) {

  const data = {
    type: 'request_news',
    type_news: category,
    page: 1
  }

  const config = {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  }

  return axios.post('https://bayarsaja.com/api/mobilecore', qs.stringify(data), config);

}