import { createStackNavigator } from 'react-navigation-stack';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import ProfileScreen from './../screens/ProfileScreen';
import DashboardScreen from './../screens/DashboardScreen';
import OrderScreen from './../screens/OrderScreen';
import NewsScreen from './../screens/NewsScreen';
import ChatScreen from './../screens/ChatScreen';

const DashboardTabNavigator = createMaterialBottomTabNavigator({
  Home: {
    screen: DashboardScreen,
    navigationOptions: {
      title: 'Home',
      headerVisible: false,
    }
  },
  Chat: {
    screen: ChatScreen,
    navigationOptions: {
      title: 'Chat',
    }
  },
  Profile: {
    screen: ProfileScreen,
    navigationOptions: {
      title: 'Profile',
    }
  }
});

const HomeNavigator = createStackNavigator({
  Tabs: {
    screen: DashboardTabNavigator,
    navigationOptions: {
      header: null,
    }
  },
  Order: {
    screen: OrderScreen,
    title: 'Order',
  },
  News: {
    screen: NewsScreen,
    navigationOptions: {
      title: 'News',
    } 
  }
}, 
{ 
  initialRouteName: 'Tabs',
},
);

export default HomeNavigator;