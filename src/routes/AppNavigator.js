import { createSwitchNavigator } from 'react-navigation';
import SplashScreen from './../screens/SplashScreen';
import LoginScreen from './../screens/LoginScreen';

import HomeNavigator from './../routes/HomeNavigator';

const AppNavigator = createSwitchNavigator({
  Splash: SplashScreen,
  Login: LoginScreen,
  Dashboard: HomeNavigator,
});

export default AppNavigator;