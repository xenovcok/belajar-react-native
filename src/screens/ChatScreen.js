import React, { Component } from 'react';
import { View, Text } from 'react-native';

import Chatbox from  './../components/Chatbox';

class ChatScreen extends React.Component {
  constructor(props) {
    super(props);
  
    this.state = {};
  }

  componentDidMount() {
   
  }

  render() {
    return (
      <Chatbox />
    );
  }
}

export default ChatScreen;