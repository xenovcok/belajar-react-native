import React, { Component } from 'react';
import colors from './../util/Colors';
import { 
  View, 
  Image, 
  TouchableOpacity, 
  Text,
  TextInput,
  Button,
  StyleSheet 
} from 'react-native';
import { connect } from 'remx';

import * as actions from './../stores/auth/authActions';
import * as store from './../stores/auth/authStore';

class LoginScreen extends React.Component {
  constructor(props) {
    super(props);
  
    this.state = {
      user: '',
      password: '',
    };
  }

  componentDidMount() {
    console.log(this.props.userData.loggedIn);
    this.props.navigation.navigate('Dashboard');
  }
  
  handleUserInput = (txt) => {
    this.setState({user: txt});
  }

  handlePassInput = (txt) => {
    this.setState({password: txt});
  }
  
  handleLogin = async (evt) => {
    actions.setLoading(true);
    await actions.checkLogin(this.state.user, this.state.password);
    console.log(this.props.userData.loggedIn);
    if(this.IsLoggedIn()) {
      this.props.navigation.navigate('Dashboard');
    }
  }

  IsLoggedIn () {
    return this.props.userData.loggedIn;
  }

  render() {
    const { isLoading, userData } = this.props;
    
    return (
      <View style={styles.container}>
        <View style={styles.logoContainer}>
          <Image style={styles.logo} source={require('./../assets/logo.png')} />
        </View>
        <View style={styles.formContainer}>
          <TextInput placeholder='Your Username' selectionColor={colors.LIMEGREEN} underlineColorAndroid={colors.LIGH_GREY} style={styles.textInput} onChangeText={this.handleUserInput} value={this.state.user} />
          <TextInput placeholder='Your Password' selectionColor={colors.LIMEGREEN} underlineColorAndroid={colors.LIGH_GREY} style={styles.textInput} onChangeText={this.handlePassInput} value={this.state.password} />
          <Button title='Login' onPress={this.handleLogin} style={{width: '100%'}}/>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    padding: 10,
  },
  logoContainer: {
    alignItems: 'center',
  },
  logo: {
    width: 120,
    height: 80,
  },
  formContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 2,
  },
  textInput: {
    height: 40,
    paddingLeft: 6,
  }
});

function mapStateToProps() {
  return {
    isLoading: store.getters.isLoading(),
    userData: store.getters.getUserDetails()
  }
}

export default connect(mapStateToProps)(LoginScreen);