import React, { Component } from 'react';
import { 
  View, 
  FlatList, 
  Text, 
  Dimensions, 
  StyleSheet, 
  Image, 
  TouchableOpacity } from 'react-native';

import { WebView } from 'react-native-webview';

import { LoaderScreen } from 'react-native-ui-lib';
import socketIOClient from 'socket.io-client';

import GridMenu from './../components/GridMenu';
import Colors from './../util/Colors';
import NewsItem from './../components/NewsItem';

import { connect } from 'remx';
import * as store from './../stores/auth/authStore';
import * as postStore from './../stores/posts/postsStore';
import * as postAction from './../stores/posts/postActions';

const width = Math.round(Dimensions.get('screen').width/2);

class DashboardScreen extends Component {
  constructor(props) {
    super(props);
    
    postAction.getPost('all');

    this.state = {
       posts: [],
       loggedIn: '',
    };

  }

  componentDidMount() {
    const { isLoading, userData } = this.props;

    //console.log('Dashboard log: '+this.props.userData.loggedIn);
    this.state.loggedIn = this.props.userData.loggedIn;
  }

  render() {
    const { posts } = this.props;
    const scalesPageToFit = true;
    console.log(this.props.posts);
    console.log('isloading', this.props.isLoading);
    return (
      <View style={styles.container}>
       <View style={[styles.item, { justifyContent: 'center' }]}>
         <Image style={styles.stretch} source={require('./../assets/logo.png')} />
       </View>
       <View style={{height: 65}}>
         <WebView 
           source={{ uri: 'file:///android_asset/HorizontalList.html' }}
           originWhitelist={['*']}
           scalesPageToFit={scalesPageToFit}
           bounces={false}
           scrollEnabled={false}
         />
       </View>
       <View style={[styles.post, { justifyContent: 'center'}]} >
          <FlatList
             style={{flex: 1}} 
             data={this.props.posts}
             keyExtractor = {this._keyExtractor}
             renderItem = {this._renderItem}
             showsHorizontalScrollIndicator={true}
          />          
       </View>
      </View>
    );
  }

  _keyExtractor = (item, index) => item._id;

  _renderItem = ({item}) => (
    <NewsItem headline={item.headline} img={item.image} excerpt={item.article} author={item.author} waktu={item.waktu} id={item._id} navigation={this.props.navigation}/>
  );
}

function mapStateToProps() {
  return {
    isLoading: store.getters.isLoading(),
    userData: store.getters.getUserDetails(),
    posts: postStore.getters.getPosts()
  }
}


export default connect(mapStateToProps)(DashboardScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    flexDirection: 'column',
    flexWrap: 'wrap',
  },
  item :{
    height: 120,
    padding: 10,
    width: '100%',
    margin: 1,
    backgroundColor: Colors.BLUE,
    // flexGrow: 1,
    // flexShrink: 0,
  },
  post : {
    height: 220,
    padding: 10,
    flex: 1,
    margin:1,
    backgroundColor: Colors.LIGH_GREY,
  },
  stretch: {
    width: '100%',
    height: '100%',
    resizeMode: 'center'
  }
});