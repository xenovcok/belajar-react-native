import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native'; 

class ProfileScreen extends React.Component {
  constructor(props) {
    super(props);
  
    this.state = {};
  }

  componentDidMount() {

  }

  render() {
    return (
      <View style={styles.container}>
        <Text>Profile Screen</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  }
});

export default ProfileScreen;