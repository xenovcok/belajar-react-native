import React, { Component } from 'react';
import { View, Text } from 'react-native';
import News from './../components/News';

class NewsScreen extends React.Component {
  constructor(props) {
    super(props);
  
    this.state = {};
  }

  componentDidMount() {

  }

  render() {
    const id = this.props.navigation.getParam('id', 'no-id');
    return (
      <News id={id} />
    );
  }
}

export default NewsScreen;