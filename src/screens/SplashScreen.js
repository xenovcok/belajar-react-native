import React, { Component } from 'react';
import { View, Image, StyleSheet } from 'react-native';

class SplashScreen extends React.Component {
  constructor(props) {
    super(props);
  
    this.state = {};
  }

  componentDidMount() {
    setTimeout(() => {
      this.props.navigation.navigate('Login')}, 3000
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <Image 
          style={[styles.item, { width: 120, height: 80 }]} 
          source={require('./../assets/logo.png')} 
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  item: {
    alignItems: 'center',
  } 
});

export default SplashScreen;