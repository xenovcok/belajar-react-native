const colors = {
  BLUE: '#9999ff',
  WHITE: '#ffffff',
  LIMEGREEN: '#32CD32',
  LIGH_GREY: '#F5F5F5',
};

export default colors;