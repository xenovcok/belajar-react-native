import * as remx from 'remx';
import _ from 'lodash';

const initialState = {
  loading: false,
  posts: [],
}

const state = remx.state(initialState);

export const getters = remx.getters({
  
  isLoading () {
    return state.loading;
  },

  getPosts() {
    return state.posts;
  },

  getPostById (id) {
    return _.find(state.posts, function (ob) { return ob._id === id; });
  }
});

export const setters = remx.setters({
  
  setLoading (val) {
    state.loading = val;
  },

  setPosts (data) {
    state.posts = data;
    state.loading = false;
  }
});