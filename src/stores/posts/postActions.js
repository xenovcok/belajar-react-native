import * as store from './postsStore';
import * as api from './../../services/bayarsajaApi';

export async function getPost (category) {
  const result = await api.getPostByCategory(category);
  store.setters.setPosts(result.data.data);
  console.log(result.data.data);
}

export async function getPostById (id) {
  const posts = store.getters.getPost;

  return posts.filter(ob => ob._id === id);
}