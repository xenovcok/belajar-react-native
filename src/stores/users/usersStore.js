import * as remx from 'remx';
import user from './../../services/api';

const initalState = {
  loading: true,
  users: []
};

const state = remx.state(initalState);

export const getters = remx.getters({
  getUser(id) {
    return state.users;
  }, 

  isLoading() {
    return state.loading;
  },
});

export const setters = remx.setters({
  setUser(user) {
    state.users = user;
    state.loading = false;
  }
});