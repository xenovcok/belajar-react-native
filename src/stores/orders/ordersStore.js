import * as remx from 'remx';

const initialState = {
  loading: true,
  userDetails: [],
}

const state = remx.state(initialState);

export const getters = remx.getters({
  
  isLoading() {
    return state.loading;
  },

  getUserDetails() {
    return state.userDetails;
  },

});

export const setters = remx.setters({
  
  setAuthDetails(details) {
    state.userDetails = details;
    state.loading = false;
  }

});