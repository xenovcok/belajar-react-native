import * as store from './authStore';
import * as api from './../../services/api';

export async function checkLogin(user, pass) {
  const data = await api.doLogin(user, pass);
  store.setters.setUserDetails(data);
}

export function setLoading(val) {
  store.setters.setLoading(val);
}