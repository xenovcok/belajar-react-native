import * as remx from 'remx';

const initialState = {
  loading: false,
  data: {
    loggedIn : false
  },
}

const state = remx.state(initialState);

export const getters = remx.getters({
  
  isLoading() {
    return state.loading;
  },

  getUserDetails() {
    return state.data;
  },

});

export const setters = remx.setters({
  
  setUserDetails(details) {
    state.data = details;
    state.loading = false;
  },

  setLoading(val) {
    state.loading = val;
  }

});