import * as remx from 'remx';

const initialState = {
  messages: [],
}

const state = remx.state(initialState);

export const getters = remx.getters({
  getMessage () {
    return state.messages;
  },
});

export const setters = remx.setters({
  setMessage (msg) {
    state.messages = msg;
  }
});