import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

class GridMenu extends React.Component {
   constructor(props) {
     super(props);
   
     this.state = {

     };
   }

   handleMenuPress = (name) => {
      this.props.navigation.navigate(name);
      console.log(this.props.navigation);
   }

   componentDidMount() {

   }

   render() {
     return (
       <View style={styles.container}>
        <View style={styles.row}>
         <TouchableOpacity style={styles.item} onPress={() => this.handleMenuPress('Order')} >
            <Text style={{color:'white'}}>Order</Text>
         </TouchableOpacity>
          <TouchableOpacity style={styles.item} onPress={() => this.handleMenuPress('Menu')} >
            <Text style={{color:'white'}}>MENU</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.row}>
          <TouchableOpacity style={styles.item} onPress={() => this.handleMenuPress('Report')} >
            <Text style={{color:'white'}}>REPORT</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.item} onPress={() => this.handleMenuPress('Logout')} >
            <Text style={{color:'white'}}>LOGOUT</Text>
          </TouchableOpacity>
        </View>
       </View>
     );
   }
}

export default GridMenu;

const styles = StyleSheet.create({
  container: {
    flex: .5,
    flexDirection: 'column',
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginBottom: 8
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10
  },
  item: {
    justifyContent: 'center',
    padding: 10,
    flex: 1,
    height: 130,
    margin: 1,
    alignItems: 'center',
    width: '49%',
    backgroundColor: 'gray'
  }
});