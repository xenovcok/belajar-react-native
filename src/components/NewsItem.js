import React, { Component } from 'react';
import {
  View,
  Text, 
  StyleSheet,
  Image,
  TouchableOpacity
} from 'react-native';

import colors from './../util/Colors';

class NewsItem extends React.Component {
  constructor(props) {
    super(props);
  
    this.state = {};
  }

  componentDidMount() {
    
  }

  render() {
    const { headline, img, excerpt, author, waktu, id, navigation } = this.props;
    const excp = this.props.excerpt;

    return (
      <TouchableOpacity onPress={() => this._onPress(this.props.id)}>
      <View style={styles.container}>
        <View style={styles.wrapper}>
          <View style={styles.img}>
            <Image style={{width: '100%', height: 80}} source={{uri: this.props.img}} />
          </View>
          <View style={styles.article}>
            <View>
              <Text style={styles.headline}>{this.props.headline}</Text>
            </View>
            <View style={styles.excerpt}>
              <Text></Text>
            </View>
            <View style={styles.contentFooter}>
              <Text style={styles.date}>{this.props.waktu}</Text>
              <Text style={styles.author}>by {this.props.author}</Text>
            </View>
          </View>
        </View>
      </View>
      </TouchableOpacity>
    );
  }

  _onPress = (id) => {
    this.props.navigation.navigate('News', {id: id});
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 1,
  },
  wrapper: {
    flexDirection: 'row',
  },
  img: {
    width: 120,
    height: 80,
    justifyContent: 'center',
    backgroundColor: colors.LIGH_GREY,
  },
  article: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: colors.LIGH_GREY,
    padding: 4,
    marginLeft: 2,
  },
  excerpt: {
    height: 20,
  },
  headline: {
    fontSize: 12,
  },
  contentFooter: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  date: {
    textAlign: 'left',
    fontSize: 11,
  },
  author: {
    textAlign: 'right',
    fontSize: 11,
  }
});

export default NewsItem;