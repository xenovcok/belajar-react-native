import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet
} from 'react-native';

import { LoaderScreen, Card } from 'react-native-ui-lib';

import { connect } from 'remx';
import * as postStore from './../stores/posts/postsStore';


class News extends React.Component {
  constructor(props) {
    super(props);
  
    this.state = {
      post: []
    };
  }

  render() {
    const { posts, isLoading, id } = this.props;
    const post = this._filterPost(this.props.posts, this.props.id);
    console.log(post.headline);
    return (
      <View style={styles.container}>
        <Text style={styles.title} >
          {post.headline}
        </Text>
      </View>
    );
  }

  _filterPost = (data, id) => {
    console.log("news data", data);
    console.log("news id", id);
    return data.filter(d => d._id === id)[0];
  }
}

function mapStateToProps () {
  return {
    posts: postStore.getters.getPosts(),
    isLoading: postStore.getters.isLoading(),
  } 
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  title: {
    alignItems: 'center',
    fontSize: 16
  }
});

export default connect(mapStateToProps)(News);