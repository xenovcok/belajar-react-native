import React, { Component } from 'react';
import { View, FlatList, Text, Dimensions, StyleSheet, ActivityIndicator, Image, TouchableOpacity } from 'react-native';
import GridMenu from './GridMenu';
import Colors from './../util/Colors';

const width = Math.round(Dimensions.get('screen').width/2);

class Dashboard extends Component {
  constructor() {
    super();
    
    this.state = {
      dataSource: {},
    };
  }

  componentDidMount() {
    var that = this;
    var items = Array.apply(null, Array(60)).map((v, i) => {
      return { id: i, src: 'http://placehold.it/200x200?text=' + (i + 1)};
    }); 
    that.setState({
      dataSource: items,
    });
  }

  render() {
    return (
      <View style={styles.container}>
       <View style={[styles.item, { justifyContent: 'center' }]}>
         <Image style={styles.stretch} source={require('./../assets/logo.png')} />
       </View>
       <View style={styles.item}>
         <GridMenu navigation={this.props.navigation}/>
       </View>
      </View>
    );
  }
}

export default Dashboard;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: '100%',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
  },
  item :{
    height: 560,
    padding: 10,
    width: '49.5%',
    margin: 1,
    backgroundColor: Colors.BLUE,
    // flexGrow: 1,
    // flexShrink: 0,
  },
  stretch: {
    width: '100%',
    height: '100%',
    resizeMode: 'center'
  }
});