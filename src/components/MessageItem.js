import React, { Component } from 'react';
import { 
  Text, 
  View,
  StyleSheet 
} from 'react-native';
import colors from './../util/Colors';

class MessageItem extends React.Component {
  constructor(props) {
    super(props);
  
    this.state = {};
  }

  render() {
    const { data } = this.props;

    return (
      <View style={styles.message}>
        <Text style={styles.date}>Today, 14:22</Text>
        <Text style={styles.contentMsg}>{this.props.data}</Text>
        <Text style={styles.status}>|</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
   message: {
     flexDirection: 'row', 
     padding: 2,
   },
   date: {
     alignItems: 'flex-start',
     fontSize: 10,
     color: colors.LIGH_GREY,
   },
   contentMsg: {
     alignItems: 'flex-end',
     fontSize: 12,
   },
   status: {
     alignItems: 'flex-end',
     fontSize: 10,
     color: colors.LIGH_GREY,
   }
});

export default MessageItem;