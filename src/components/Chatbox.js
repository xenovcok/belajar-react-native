import React, { Component } from 'react';
import { View, StyleSheet, FlatList, TextInput, Button, KeyboardAvoidingView, Platform } from 'react-native';
import colors from './../util/Colors';
import io from 'socket.io-client';

import MessageItem from './MessageItem';

class Chatbox extends React.Component {
  constructor(props) {
    super(props);
  
    this.state = {
      message: '',
      conversation: [
      { 
        id: 1,
        content: 'hello'
      },
      {
        id: 2,
        content: 'hallo juga'
      }
      ]
    };
  }

  _renderItem = ({item}) => (
     <MessageItem 
        data={item.content}
     />
  );

  _keyExtractor = (item, index) => item.id.toString();

  handleOnChange = (val) => {
    this.setState({
      message: val
    });
  };

  sendMessage = () => {
    //this.ws.send(JSON.stringify({id: 14, to: 15, msg: this.state.message}));
    this.socket.emit('message', this.state.message);
    this.setState({
      message: ''
    });
    this.state.conversation.push({id: Math.random(100), content: this.state.message});
  }

  _onReceiveMessage = () => {

  }

  //ws = new WebSocket('ws://10.5.51.250:8080');
  socket = io('http://10.5.51.250:8080');

  componentDidMount() {
     this.socket.emit("join", (data) => console.log('emiting join'));
     this.socket.on("message", (data) => console.info(data));
     /*
     this.ws.onopen = () => {
       console.log('connection established');
     }

     this.ws.onclose = () => {
        console.log('disconnected');
     }*/
  }

  render() {
    return (
      <View style={styles.chatboxContainer}>
        <KeyboardAvoidingView style={styles.container} keyboardVerticalOffset={Platform.select({ios: () => 0, android: () => 80})()} behavior="padding" enabled>
          <FlatList
            style={styles.chatList}
            data={this.state.conversation}
            keyExtractor={this._keyExtractor}
            renderItem={this._renderItem}
          />
          
          <View style={styles.inputContainer}>
            <TextInput placeholder="Message" underlineColorAndroid={colors.LIMEGREEN} style={styles.inputMsg} onChangeText={this.handleOnChange} value={this.state.message}/>
            <Button style={styles.btnMsg} title="Send" onPress={this.sendMessage}/>
          </View>
        </KeyboardAvoidingView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  chatboxContainer: {
    flex: 1
  },
  inputContainer: {
    height: 40,
    marginBottom: 10,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    padding: 4,
  },
  inputMsg: {
    padding: 2,
    height: 40,
    flexGrow: 1,
    flex: 1
  },
  btnMsg: {
    padding: 2,
    height: 40,
    color: colors.BLUE,
  },
  chatList: {
    flex: 2
  },
});

export default Chatbox;