import React, { Component } from 'react';
import { View, Text} from 'react-native';
import AppNavigator from './src/routes/AppNavigator';
import { createAppContainer } from 'react-navigation';

const AppContainer = createAppContainer(AppNavigator);

export default class App extends Component {
  
  render () {
    return <AppContainer />;
  }
}